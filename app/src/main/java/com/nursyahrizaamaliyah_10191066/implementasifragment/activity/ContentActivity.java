package com.nursyahrizaamaliyah_10191066.implementasifragment.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.nursyahrizaamaliyah_10191066.implementasifragment.R;
import com.nursyahrizaamaliyah_10191066.implementasifragment.adapter.FragmentAdapter;

public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        TabLayout tabLayout = findViewById(R.id.contentTabLayout);
        ViewPager viewPager = findViewById(R.id.contentViewPager);

        tabLayout.addTab(tabLayout.newTab().setText("Home"));
        tabLayout.addTab(tabLayout.newTab().setText("Status"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);

        FragmentAdapter adapter = new FragmentAdapter(this, tabLayout.getTabCount(), getSupportFragmentManager());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}